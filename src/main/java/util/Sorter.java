package util;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;

import java.util.Collections;
import java.util.Comparator;

public class Sorter {
    public static final Comparator<Flower>
            SORT_FLOWERS_BY_NAME = (o1, o2) -> {
                String dev1 = o1.getName();
                String dev2 = o2.getName();
                return dev1.compareTo(dev2);
            };

    public static final Comparator<Flower>
            SORT_FLOWERS_BY_SOIL = (o1, o2) -> {
                String dev1 = o1.getSoil();
                String dev2 = o2.getSoil();
                return dev1.compareTo(dev2);
            };
    public static final Comparator<Flower>
            SORT_FLOWERS_BY_ORIGIN = (o1, o2) -> {
                String dev1 = o1.getOrigin();
                String dev2 = o2.getOrigin();
                return dev1.compareTo(dev2);
            };

    public static void sortFlowersByOrigin(final Flowers flowers) {
        flowers.getFlower().sort(SORT_FLOWERS_BY_ORIGIN);
    }
    public static void sortFlowersByName(final Flowers flowers) {
        flowers.getFlower().sort(SORT_FLOWERS_BY_NAME);
    }
    public static void sortFlowersBySoil(final Flowers flowers) {
        flowers.getFlower().sort(SORT_FLOWERS_BY_SOIL);
    }

    protected Sorter() {

    }
}
