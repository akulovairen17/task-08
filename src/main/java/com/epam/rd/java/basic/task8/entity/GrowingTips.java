package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "growingTips",propOrder = {"tempreture", "lighting", "watering"})
public class GrowingTips {

    @XmlElement(name = "tempreture",required = true)
    @XmlSchemaType(name = "positiveInteger")
    private Tempreture tempreture;

    @XmlElement(name = "lighting",required = true)
    @XmlSchemaType(name = "string")
    private Lighting lighting;

    @XmlElement(name = "watering",required = true)
    @XmlSchemaType(name = "positiveInteger")
    private Watering watering;

    public Tempreture getTempreture() {
        return tempreture;
    }

    public void setTempreture(Tempreture tempreture) {
        this.tempreture = tempreture;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }
}
