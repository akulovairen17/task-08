package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;

    private String currentElement;

    private Flowers flowers;

    private Flower flower;

    private VisualParameters visualParameters;

    private GrowingTips growingTips;


    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public final void parse(final boolean validate)
            throws ParserConfigurationException, SAXException, IOException {

        SAXParserFactory factory = SAXParserFactory.newInstance();

        factory.setNamespaceAware(true);

        if (validate) {
            factory.setFeature("http://xml.org/sax/features/validation", true);
            factory.setFeature("http://apache.org/xml/features/validation/schema",
                    true);
        }

        SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentElement = localName;
        if (currentElement.equals("flowers")) {
            flowers = new Flowers();
            return;
        }
        if (currentElement.equals("flower")) {
            flower = new Flower();
            return;
        }
        if (currentElement.equals("visualParameters")) {
            visualParameters = new VisualParameters();
            return;
        }
        if (currentElement.equals("aveLenFlower")) {
            AveLenFlower aveLenFlower = new AveLenFlower();
            aveLenFlower.setMeasure(attributes.getValue("measure"));
            visualParameters.setAveLenFlower(aveLenFlower);
            return;
        }
        if (currentElement.equals("growingTips")) {
            growingTips = new GrowingTips();
            return;
        }
        if (currentElement.equals("tempreture")) {
            Tempreture tempreture = new Tempreture();
            tempreture.setMeasure(attributes.getValue("measure"));
            growingTips.setTempreture(tempreture);
            return;
        }
        if (currentElement.equals("watering")) {
            Watering watering = new Watering();
            watering.setMeasure(attributes.getValue("measure"));
            growingTips.setWatering(watering);
            return;
        }
        if (currentElement.equals("lighting")) {
            Lighting lighting = new Lighting();
            lighting.setLightRequiring(attributes.getValue("lightRequiring"));
            growingTips.setLighting(lighting);
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (localName.equals("flower")) {
            flowers.addFlower(flower);
            flower = null;
            return;
        }
        if (localName.equals("visualParameters")) {
            flower.setVisualParameters(visualParameters);
            visualParameters = null;
            return;
        }
        if (localName.equals("growingTips")) {
            flower.setGrowingTips(growingTips);
            growingTips = null;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length)  {
        String elementText = new String(ch, start, length).trim();

        if (elementText.isEmpty()) {
            return;
        }
        if (currentElement.equals("name")) {
            flower.setName(elementText);
            return;
        }
        if (currentElement.equals("origin")) {
            flower.setOrigin(elementText);
            return;
        }
        if (currentElement.equals("soil")) {
            flower.setSoil(elementText);
            return;
        }
        if (currentElement.equals("multiplying")) {
            flower.setMultiplying(elementText);
            return;
        }
        if (currentElement.equals("stemColour")) {
            visualParameters.setStemColour(elementText);
            return;
        }
        if (currentElement.equals("leafColour")) {
            visualParameters.setLeafColour(elementText);
            return;
        }
        if (currentElement.equals("aveLenFlower")) {
            visualParameters.getAveLenFlower().setValue(elementText);
            return;
        }
        if (currentElement.equals("tempreture")) {
            growingTips.getTempreture().setValue(Integer.valueOf(elementText));
            return;
        }
        if (currentElement.equals("watering")) {
            growingTips.getWatering().setValue(Integer.valueOf(elementText));
        }
    }

    public final Flowers getFlowers() {
        return flowers;
    }

    public final void setFlowers(final Flowers flowers2) {
        this.flowers = flowers2;
    }
}
