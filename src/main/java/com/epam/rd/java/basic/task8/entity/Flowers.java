package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"flower"})
@XmlRootElement(name = "flowers")
public class Flowers {
    private List<Flower> flower;

    @XmlElement(required = true)
    public List<Flower> getFlower() {
        return flower;
    }

    public Flowers() {
        flower = new ArrayList<>();
    }

    public final void addFlower(final Flower newFlower) {
        flower.add(newFlower);
    }
}
