package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "flower", propOrder = {"name","soil","origin"} )

public class Flower {
    @XmlElement(name = "name",required = true)
    @XmlSchemaType(name = "string")
    private String name;

    @XmlElement(name = "soil",required = true)
    @XmlSchemaType(name = "string")
    private String soil;

    @XmlElement(name = "origin",required = true)
    @XmlSchemaType(name = "string")
    private String origin;

    @XmlElement(name = "visualParameters",required = true)
    private VisualParameters visualParameters;

    @XmlElement(name = "growingTips",required = true)
    private GrowingTips growingTips;

    @XmlElement(name = "multiplying",required = true)
    @XmlSchemaType(name = "string")
    private String multiplying;


    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                '}';
    }
}
