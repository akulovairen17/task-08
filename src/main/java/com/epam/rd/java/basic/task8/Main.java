package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;
import org.xml.sax.SAXException;
import util.Sorter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		try {
			domController.parse(true);
			Flowers flowers = domController.getFlowers();

			// sort (case 1)
			Sorter.sortFlowersByName(flowers);

			// save
			String outputXmlFile = "output.dom.xml";
			DOMController.saveToXML(flowers, outputXmlFile);

			// //////////////////////////////////////////////////////
			// SAX
			// //////////////////////////////////////////////////////

			// get
			System.out.println("Output ==> " + outputXmlFile);
			SAXController saxController = new SAXController(xmlFileName);
			saxController.parse(true);
			flowers = saxController.getFlowers();

			// sort (case 2)
			Sorter.sortFlowersByOrigin(flowers);

			// save
			outputXmlFile = "output.sax.xml";

			// other way:
			DOMController.saveToXML(flowers, outputXmlFile);
			System.out.println("Output ==> " + outputXmlFile);

			// //////////////////////////////////////////////////////
			// StAX
			// //////////////////////////////////////////////////////

			// get
			STAXController staxController = new STAXController(xmlFileName);
			staxController.parse();

			flowers = staxController.getFlowers();

//			 sort (case 3)
			Sorter.sortFlowersBySoil(flowers);

//			 save
			outputXmlFile = "output.stax.xml";
			DOMController.saveToXML(flowers, outputXmlFile);
			System.out.println("Output ==> " + outputXmlFile);

		} catch (ParserConfigurationException | SAXException | IOException | TransformerException | XMLStreamException e) {
			System.err.println(e.getMessage());
		}

	}
}
