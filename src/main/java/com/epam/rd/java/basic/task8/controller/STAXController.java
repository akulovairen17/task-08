package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;

    private Flowers flowers;

    private Flower flower;

    private VisualParameters visualParameters;

    private GrowingTips growingTips;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public final void parse() throws ParserConfigurationException,
            SAXException, IOException, XMLStreamException {

        String currentElement = null;

        XMLInputFactory factory = XMLInputFactory.newInstance();

        factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

        XMLEventReader reader = factory.createXMLEventReader(new StreamSource(
                xmlFileName));

        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();

            if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
                continue;
            }
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                currentElement = startElement.getName().getLocalPart();
                if (currentElement.equals("flowers")) {
                    flowers = new Flowers();
                }
                if (currentElement.equals("flower")) {
                    flower = new Flower();
                }
                if (currentElement.equals("visualParameters")) {
                    visualParameters = new VisualParameters();
                }
                if (currentElement.equals("aveLenFlower")) {
                    Attribute attribute = startElement
                            .getAttributeByName(new QName("measure"));
                    if (attribute != null) {
                        AveLenFlower aveLenFlower = new AveLenFlower();
                        aveLenFlower.setMeasure(attribute.getValue());
                        visualParameters.setAveLenFlower(aveLenFlower);
                    }
                }
                if (currentElement.equals("growingTips")) {
                    growingTips = new GrowingTips();
                }
                if (currentElement.equals("tempreture")) {
                    Attribute attribute = startElement.getAttributeByName(new QName("measure"));
                    if (attribute != null) {
                        Tempreture tempreture = new Tempreture();
                        tempreture.setMeasure(attribute.getValue());
                        growingTips.setTempreture(tempreture);
                    }
                }
                if (currentElement.equals("watering")) {
                    Attribute attribute = startElement.getAttributeByName(new QName("measure"));
                    if (attribute != null) {
                        Watering watering = new Watering();
                        watering.setMeasure(attribute.getValue());
                        growingTips.setWatering(watering);
                    }
                }
                if (currentElement.equals("lighting")) {
                    Attribute attribute = startElement.getAttributeByName(new QName("lightRequiring"));
                    if (attribute != null) {
                        Lighting lighting = new Lighting();
                        lighting.setLightRequiring(attribute.getValue());
                        growingTips.setLighting(lighting);
                    }
                }
            }
            if (event.isCharacters()) {
                Characters characters = event.asCharacters();
                String elementText = characters.getData();
                if (currentElement.equals("name")) {
                    flower.setName(elementText);
                }
                if (currentElement.equals("origin")) {
                    flower.setOrigin(elementText);
                }
                if (currentElement.equals("soil")) {
                    flower.setSoil(elementText);
                }
                if (currentElement.equals("multiplying")) {
                    flower.setMultiplying(elementText);
                }
                if (currentElement.equals("stemColour")) {
                    visualParameters.setStemColour(elementText);
                }
                if (currentElement.equals("leafColour")) {
                    visualParameters.setLeafColour(elementText);
                }
                if (currentElement.equals("aveLenFlower")) {
                    visualParameters.getAveLenFlower().setValue(elementText);
                }
                if (currentElement.equals("tempreture")) {
                    growingTips.getTempreture().setValue(Integer.valueOf(elementText));
                }
                if (currentElement.equals("watering")) {
                    growingTips.getWatering().setValue(Integer.valueOf(elementText));
                }
            }
            if (event.isEndElement()) {
                EndElement endElement = event.asEndElement();
                String localName = endElement.getName().getLocalPart();

                if (localName.equals("flower")) {
                    flowers.addFlower(flower);
                    flower = null;
                }
                if (localName.equals("visualParameters")) {
                    flower.setVisualParameters(visualParameters);
                    visualParameters = null;
                }
                if (localName.equals("growingTips")) {
                    flower.setGrowingTips(growingTips);
                    growingTips = null;
                }
            }
        }
        reader.close();
    }
    public final Flowers getFlowers() {
        return flowers;
    }
}