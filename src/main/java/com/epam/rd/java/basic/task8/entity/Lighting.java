package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lighting", propOrder = {"lighting"} )
public class Lighting {
    @XmlValue
    private String value;

    @XmlAttribute(name = "lightRequiring")
    private String lightRequiring;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }
}
