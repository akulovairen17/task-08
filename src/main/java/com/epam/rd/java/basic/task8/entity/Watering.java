package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "watering", propOrder = {"watering"} )
public class Watering {
    @XmlValue
    private Integer value;

    @XmlAttribute(name = "measure")
    private String measure;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }
}
