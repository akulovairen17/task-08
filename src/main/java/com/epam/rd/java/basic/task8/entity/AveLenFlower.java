package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aveLenFlower", propOrder = {"aveLenFlower"} )
public class AveLenFlower {
    @XmlValue
    private String value;

    @XmlAttribute(name = "measure")
    private String measure;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }
}
