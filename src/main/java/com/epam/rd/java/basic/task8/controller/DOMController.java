package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private Flowers flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public final void parse(final boolean validate)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);

		if (validate) {
			factory.setFeature("http://xml.org/sax/features/validation", true);
			factory.setFeature("http://apache.org/xml/features/validation/schema", true);
		}
		DocumentBuilder db = factory.newDocumentBuilder();
		db.setErrorHandler(new DefaultHandler() {
			public void error(final SAXParseException e) throws SAXException {
				throw e;
			}
		});


		Document document = db.parse(xmlFileName);
		Element root = document.getDocumentElement();

		flowers = new Flowers();
		NodeList flowerNodes = root.getElementsByTagName("flower");

		for (int j = 0; j < flowerNodes.getLength(); j++) {
			Flower flower = getFlower(flowerNodes.item(j));
			flowers.addFlower(flower);
		}
	}

	private Flower getFlower(final Node qNode){
		Flower flower=new Flower();
		Element qElement = (Element) qNode;
		Node qName = qElement.getElementsByTagName("name").item(0);
		flower.setName(qName.getTextContent());

		Node qSoil = qElement.getElementsByTagName("soil").item(0);
		flower.setSoil(qSoil.getTextContent());

		Node qOrigin = qElement.getElementsByTagName("origin").item(0);
		flower.setOrigin(qOrigin.getTextContent());

		flower.setVisualParameters(getVisualParameters(qElement.getElementsByTagName("visualParameters").item(0)));
		flower.setGrowingTips(getGrowingTips(qElement.getElementsByTagName("growingTips").item(0)));

		Node qMultiplying = qElement.getElementsByTagName("multiplying").item(0);
		flower.setMultiplying(qMultiplying.getTextContent());

		return flower;
	}

	public VisualParameters getVisualParameters(final Node qNode){
		VisualParameters visualParameters=new VisualParameters();
		Element qElement= (Element) qNode;

		Node qStemColour = qElement.getElementsByTagName("stemColour").item(0);
		visualParameters.setStemColour(qStemColour.getTextContent());

		Node qLeafColour = qElement.getElementsByTagName("leafColour").item(0);
		visualParameters.setLeafColour(qLeafColour.getTextContent());

		Element qAveLenFlower = (Element) qElement.getElementsByTagName("aveLenFlower").item(0);
		AveLenFlower aveLenFlower = new AveLenFlower();
		aveLenFlower.setMeasure(qAveLenFlower.getAttribute("measure"));
		aveLenFlower.setValue(qAveLenFlower.getTextContent());
		visualParameters.setAveLenFlower(aveLenFlower);

		return visualParameters;
	}

	private GrowingTips getGrowingTips (final Node qNode){
		GrowingTips growingTips=new GrowingTips();
		Element qElement = (Element) qNode;

		Element qTempreture = (Element) qElement.getElementsByTagName("tempreture").item(0);
		Tempreture tempreture = new Tempreture();
		tempreture.setValue(Integer.valueOf(qTempreture.getTextContent()));
		tempreture.setMeasure(qTempreture.getAttribute("measure"));
		growingTips.setTempreture(tempreture);

		Element qLighting = (Element) qElement.getElementsByTagName("lighting").item(0);
		Lighting lighting = new Lighting();
		lighting.setLightRequiring(qLighting.getAttribute("lightRequiring"));
		growingTips.setLighting(lighting);

		Element qWatering = (Element) qElement.getElementsByTagName("watering").item(0);
		Watering watering = new Watering();
		watering.setValue(Integer.valueOf(qWatering.getTextContent()));
		watering.setMeasure(qWatering.getAttribute("measure"));
		growingTips.setWatering(watering);

		return growingTips;
	}

	public static Document getDocument(final Flowers flowers)
			throws ParserConfigurationException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		dbf.setNamespaceAware(true);

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		Element tElement = document.createElement("flowers");
		tElement.setAttribute("xmlns", "http://www.nure.ua");
		tElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance" );
		tElement.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd " );

		document.appendChild(tElement);
		for (Flower flower : flowers.getFlower()) {

			Element fElement = document.createElement("flower");
			tElement.appendChild(fElement);

			Element nElement = document.createElement("name");
			nElement.setTextContent(flower.getName());
			fElement.appendChild(nElement);

			Element sElement = document.createElement("soil");
			sElement.setTextContent(flower.getSoil());
			fElement.appendChild(sElement);

			Element oElement = document.createElement("origin");
			oElement.setTextContent(String.valueOf(flower.getOrigin()));
			fElement.appendChild(oElement);

			Element visualParametersElement = document.createElement("visualParameters");
			fElement.appendChild(visualParametersElement);

			Element stemColourElement = document.createElement("stemColour");
			stemColourElement.setTextContent(flower.getVisualParameters().getStemColour());
			visualParametersElement.appendChild(stemColourElement);

			Element leafColourElement = document.createElement("leafColour");
			leafColourElement.setTextContent(flower.getVisualParameters().getLeafColour());
			visualParametersElement.appendChild(leafColourElement);

			Element aveLenFlowerElement = document.createElement("aveLenFlower");
			AveLenFlower aveLenFlower = flower.getVisualParameters().getAveLenFlower();
			aveLenFlowerElement.setAttribute("measure", aveLenFlower.getMeasure());
			aveLenFlowerElement.setTextContent(aveLenFlower.getValue());
			visualParametersElement.appendChild(aveLenFlowerElement);

			Element growingTipsElement = document.createElement("growingTips");
			fElement.appendChild(growingTipsElement);

			Element tempretureElement = document.createElement("tempreture");
			Tempreture tempreture = flower.getGrowingTips().getTempreture();
			tempretureElement.setAttribute("measure", tempreture.getMeasure());
			tempretureElement.setTextContent(String.valueOf(tempreture.getValue()));
			growingTipsElement.appendChild(tempretureElement);

			Element lightingElement = document.createElement("lighting");
			lightingElement.setAttribute("lightRequiring", flower.getGrowingTips().getLighting().getLightRequiring());
			growingTipsElement.appendChild(lightingElement);

			Element wateringElement = document.createElement("watering");
			Watering watering = flower.getGrowingTips().getWatering();
			wateringElement.setAttribute("measure", watering.getMeasure());
			wateringElement.setTextContent(String.valueOf(watering.getValue()));
			growingTipsElement.appendChild(wateringElement);

			Element oMultiplying = document.createElement("multiplying");
			oMultiplying.setTextContent(String.valueOf(flower.getMultiplying()));
			fElement.appendChild(oMultiplying);
		}
		return document;
	}

	public static void saveToXML(final Flowers flowers,
								 final String xmlFileName)
			throws ParserConfigurationException, TransformerException {
		saveToXML(getDocument(flowers), xmlFileName);
	}

	public static void saveToXML(final Document document,
								 final String xmlFileName) throws TransformerException {

		StreamResult result = new StreamResult(new File(xmlFileName));

		Transformer tf = TransformerFactory.newInstance().newTransformer();
//		javax.xml.transform.Transformer t = tf.newTransformer();
		tf.setOutputProperty(OutputKeys.INDENT, "yes");

		try {
			tf.transform(new DOMSource(document), new StreamResult(new FileOutputStream(xmlFileName)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	public final Flowers getFlowers() {
		return flowers;
	}
}


