package com.epam.rd.java.basic.task8.entity;


import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "visualParameters",propOrder = {"stemColour", "leafColour", "aveLenFlower"})
public class VisualParameters {
    @XmlElement(name = "stemColour",required = true)
    @XmlSchemaType(name = "string")
    private String stemColour;

    @XmlElement(name = "leafColour",required = true)
    @XmlSchemaType(name = "string")
    private String leafColour;

    @XmlElement(name = "aveLenFlower",required = true)
    @XmlSchemaType(name = "positiveInteger")
    private AveLenFlower aveLenFlower;

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }
}
